package concurrency;

import java.util.concurrent.TimeUnit;

public class ExtLib {
    public int eval(int a, int power) {
        int result = 1;
        for (int i = 0; i < power; i++) {
            result *= a;
        }
        result += delay();//всегда вернет 0
        //sleepDelay();
        // не используется, так как при тестах производительности
        // на одноядерной системе вычисления выполнялись бы все равно в X раз быстрее
        return result;
    }

    private void sleepDelay() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //такой запутанный, чтобы java не оптимизировала код
    private int delay() {
        double d = 0;
        for (int i = 0; i < 100_000; i++) {
            for (int j = 0; j < 100_000; j++) {
                if (i == 1) {
                    if (j == 1) {
                        d = Math.sqrt(i);
                    }
                }
            }
        }
        if (d == 1.2) {
            return 1;
        } else {
            return 0;
        }

    }
}
