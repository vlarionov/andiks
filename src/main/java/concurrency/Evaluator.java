package concurrency;

public interface Evaluator {
    int[] evaluate(int[] data, int power);
}
