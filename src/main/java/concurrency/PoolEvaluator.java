package concurrency;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class PoolEvaluator implements Evaluator {
    private int coreNumber;
    private int batchSize;
    private ExtLib extLib;

    public PoolEvaluator(int coreNumber, int batchSize, ExtLib extLib) {
        this.coreNumber = coreNumber;
        this.batchSize = batchSize;
        this.extLib = extLib;
    }

    public int[] evaluate(int[] data, int power) {
        try {
            int[] resultData = new int[data.length];
            ExecutorService evaluators = Executors.newFixedThreadPool(coreNumber);
            AtomicInteger nextBatchIndex = new AtomicInteger();
            CountDownLatch finishLatch = new CountDownLatch(coreNumber);
            for (int i = 0; i < coreNumber; i++) {
                evaluators.submit(new AtomicEvaluationTask(data, resultData, nextBatchIndex, power, finishLatch));
            }
            finishLatch.await();
            return resultData;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class AtomicEvaluationTask implements Runnable {
        private final int[] data;
        private final int[] resultData;
        private final AtomicInteger nextBatchIndex;
        private final int power;
        private final CountDownLatch finishLatch;

        public AtomicEvaluationTask(int[] data, int[] resultData, AtomicInteger nextBatchIndex, int power,
                                    CountDownLatch finishLatch) {
            this.data = data;
            this.resultData = resultData;
            this.nextBatchIndex = nextBatchIndex;
            this.power = power;
            this.finishLatch = finishLatch;
        }

        public void run() {
            for (int i = nextBatchIndex.getAndAdd(batchSize); i < data.length; i = nextBatchIndex.getAndAdd(batchSize)) {
                for (int j = 0; (j < batchSize) && (j + i < data.length); j++) {
                    resultData[i + j] = extLib.eval(data[i + j], power);
                }
            }
            finishLatch.countDown();
        }
    }
}
