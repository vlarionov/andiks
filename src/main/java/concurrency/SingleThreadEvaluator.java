package concurrency;

public class SingleThreadEvaluator implements Evaluator {
    private ExtLib extLib;

    public SingleThreadEvaluator(ExtLib extLib) {
        this.extLib = extLib;
    }

    @Override
    public int[] evaluate(int[] data, int power) {
        int length = data.length;
        int[] result = new int[length];
        for (int i = 0; i < length; i++) {
            result[i] = extLib.eval(data[i], power);
        }
        return result;
    }
}
