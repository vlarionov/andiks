package concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class TaskEvaluator implements Evaluator {
    private int coreNumber;
    private ExtLib extLib;

    public TaskEvaluator(int coreNumber, ExtLib extLib) {
        this.coreNumber = coreNumber;
        this.extLib = extLib;
    }

    public int[] evaluate(int[] data, int power) {
        try {
            ExecutorService evaluators = Executors.newFixedThreadPool(coreNumber);
            List<SegmentEvaluationTask> tasks = createTasks(data, power);
            List<Future<TaskResult>> results = evaluators.invokeAll(tasks);
            return getResult(results, data.length);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    private int[] getResult(List<Future<TaskResult>> results, int dataLength) throws ExecutionException, InterruptedException {
        int[] result = new int[dataLength];
        for (Future<TaskResult> taskResultFuture : results) {
            TaskResult taskResult = taskResultFuture.get();
            System.arraycopy(taskResult.getResultData(), 0, result, taskResult.getIndexFrom(), taskResult.getLength());
        }
        return result;
    }

    private List<SegmentEvaluationTask> createTasks(int[] data, int power) {
        List<SegmentEvaluationTask> tasks = new ArrayList<>();
        int dataLength = data.length;
        int indexFrom = 0;
        for (int i = 0; i < coreNumber; i++) {
            int length = (dataLength / coreNumber) + (i < dataLength % coreNumber ? 1 : 0);
            SegmentEvaluationTask task = new SegmentEvaluationTask(data, power, indexFrom, length, extLib);
            tasks.add(task);
            indexFrom += length;
        }
        return tasks;
    }

    private class TaskResult {
        private final int[] resultData;
        private final int indexFrom;
        private final int length;

        public TaskResult(int[] resultData, int indexFrom, int length) {
            this.resultData = resultData;
            this.indexFrom = indexFrom;
            this.length = length;
        }

        public int[] getResultData() {
            return resultData;
        }

        public int getIndexFrom() {
            return indexFrom;
        }

        public int getLength() {
            return length;
        }
    }

    private class SegmentEvaluationTask implements Callable<TaskResult> {
        private final int[] data;
        private final int power;
        private final int indexFrom;
        private final int length;
        private final ExtLib extLib;

        public SegmentEvaluationTask(int[] data, int power, int indexFrom, int length, ExtLib extLib) {
            this.data = data;
            this.power = power;
            this.indexFrom = indexFrom;
            this.length = length;
            this.extLib = extLib;
        }

        public TaskResult call() throws Exception {
            int[] resultData = new int[length];
            TaskResult taskResult = new TaskResult(resultData, indexFrom, length);
            for (int i = 0; i < length; i++) {
                resultData[i] = extLib.eval(data[indexFrom + i], power);
            }
            return taskResult;
        }
    }
}
