package algorithms;

public enum Coin {
    PENNY(1), NICKEL(5), DIME(10), QUARTER(25), HALF_DOLLAR(50);

    private int faceValue;

    Coin(int faceValue) {
        this.faceValue = faceValue;
    }

    public int getFaceValue() {
        return faceValue;
    }

    public static int countWaysToProduceGivenAmountOfMoney(int amount) {
        if (amount < 1) {
            return 0;
        } else {
            return countWaysToProduceGivenAmountOfMoney(amount, HALF_DOLLAR);
        }
    }

    private static int countWaysToProduceGivenAmountOfMoney(int residualAmount, Coin currentCoin) {
        if (currentCoin == PENNY) {
            return 1;
        } else {
            int subWaysNumber = 0;
            for (int i = 0; i * currentCoin.getFaceValue() < residualAmount; i++) {
                subWaysNumber += countWaysToProduceGivenAmountOfMoney(
                        residualAmount - i * currentCoin.getFaceValue(), Coin.values()[currentCoin.ordinal() - 1]);
            }
            return subWaysNumber;
        }
    }

}
