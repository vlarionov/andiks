package algoritms;

import algorithms.Coin;
import org.junit.Assert;
import org.junit.Test;

public class CoinTest {
    @Test
    public void testCountWaysToProduceGivenAmountOfMoney() {
        int result = Coin.countWaysToProduceGivenAmountOfMoney(11);
        Assert.assertEquals(4, result);
    }
}
