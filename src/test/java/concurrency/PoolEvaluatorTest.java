package concurrency;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

public class PoolEvaluatorTest {
    private final static int CORE_NUMBER = 4;
    private final static int DATA_SIZE = 1_000_000;
    private final static int MAX_DATA_VALUE = 10;
    private final static int DEFAULT_POWER = 9;

    private final static int PACKAGE_SIZE = 100;

    private static PoolEvaluator poolEvaluator;
    private static SingleThreadEvaluator singleThreadEvaluator;
    private static Random random;
    private static int[] data;

    @BeforeClass
    public static void setUpClass() {
        ExtLib extLib = new ExtLib();
        poolEvaluator = new PoolEvaluator(CORE_NUMBER, PACKAGE_SIZE, extLib);
        singleThreadEvaluator = new SingleThreadEvaluator(extLib);
        random = new Random();

        data = new int[DATA_SIZE];
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(MAX_DATA_VALUE);
        }
    }

    @Test
    public void testPerformanceForPoolEvaluator() {
        System.out.println(evaluateWithTimer(poolEvaluator, data, DEFAULT_POWER));
    }

    @Ignore
    @Test
    public void testPerformanceForSimpleEvaluator() {
        System.out.println(evaluateWithTimer(singleThreadEvaluator, data, DEFAULT_POWER));
    }

    //так как jvm оптимизирует использование памяти, второй раз вычисления выполнились бы намного быстрее
    @Ignore
    @Test
    public void testEvaluate() {
        int[] poolEvaluatorResult = poolEvaluator.evaluate(data, DEFAULT_POWER);
        int[] simpleEvaluatorResult = singleThreadEvaluator.evaluate(data, DEFAULT_POWER);

        Assert.assertTrue(Arrays.equals(poolEvaluatorResult, simpleEvaluatorResult));
    }

    private EvaluationResult evaluateWithTimer(Evaluator evaluator, int[] data, int power) {
        long startTime = System.currentTimeMillis();
        int[] resultData = evaluator.evaluate(data, power);
        long evaluatingTime = System.currentTimeMillis() - startTime;
        return new EvaluationResult(resultData, evaluatingTime, evaluator.getClass().getName());
    }

    private class EvaluationResult {
        private int[] resultData;
        private long timeInMillis;
        private String evaluatorClassName;

        public EvaluationResult(int[] resultData, long timeInMillis, String evaluatorClassName) {
            this.resultData = resultData;
            this.timeInMillis = timeInMillis;
            this.evaluatorClassName = evaluatorClassName;
        }

        @Override
        public String toString() {
            return "Evaluator: " + evaluatorClassName + "; time(milliseconds): " + timeInMillis;
        }
    }
}
